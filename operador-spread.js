// function(a, b, c)
let nums = [1, 3, 6];
function sumar(a, b, c) {
    console.log(a + b + c);
}
sumar(...nums);
  
// function(n parámetros)
let a = 3;
let b = 7;
let c = 8;

function sumar(...nums) {
  let suma = 0;
  for (n of nums) {
    suma += n;
  }
  console.log("La suma es " + suma);
}
sumar(a, b, c);

// push y unshift
let nums1 = [1, 3, 6];
let nums2 = [0, 52, 15, 9];

nums1.push(...nums2);
console.log(nums1);
nums1.unshift(...nums2);
console.log(nums1);

// meter un array en medio de otra
let nums1 = [1, 3, 6];
let nums2 = [0, 52, 15, 9];
nums1.splice(1, 0, ...nums2);
console.log(nums1);

// copiar un array
let nums1 = [1, 3, 6];
let nums2 = [...nums1];

// mergear un objeto con defaults (ES2018)
let defaults = {
  ancho: 100,
  alto: 200,
  color: "rojo" 
}

let o1 = {
  ancho: 200,
  grosor: 10000
}

o1 = { ...defaults, ...o1 };

// convertir un NodeList a un array
let lis = [...document.getElementsByTagName("li")];
